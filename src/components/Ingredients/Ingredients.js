import React, { useEffect, useCallback, useReducer,useMemo } from 'react';

import IngredientForm from './IngredientForm';
import IngredientList from './IngredientList'
import ErrorModal from '../UI/ErrorModal';
import Search from './Search';

const ingredientReducer = (currentIngredients, action) => {
  switch (action.type) {
    case 'SET':
      return action.ingredients;
    case 'ADD':
      return [...currentIngredients, action.ingredient];
    case 'DELETE':
      return currentIngredients.filter(ing => ing.id !== action.id);
    case 'INITIAL':
      return action.ingredients;
    default:
      throw new Error('should no get here!!');
  }
}

const httpReducer = (currentHttpState, action) => {
  switch (action.type) {
    case 'SEND':
      return { loading: true, error: null };
    case 'RESPONSE':
      return { ...currentHttpState, loading: false };
    case 'ERROR':
      return { loading: true, error: action.errorData };
    case 'CLEAR':
      return { ...currentHttpState, error: null };
    default:
      throw new Error('should no get here!!');
  }
}

function Ingredients() {
  const [userIngredients, dispatch] = useReducer(ingredientReducer, []);
  const [httpState, dispatchHttp] = useReducer(httpReducer, { loading: false, error: null });

  //works like a componentdidmount with 2nd parameter [] or else works as componentDidUpdate
  useEffect(() => {
    fetch('https://react-hooks-530fe.firebaseio.com/ingredients.json', {
      headers: { "Access-Control-Allow-Credentials": true }
    })
      .then(response => {
        return response.json();
      })
      .then(responseData => {
        console.log(responseData);
        const loadedIngredients = [];
        for (const key in responseData) {
          loadedIngredients.push({
            id: key,
            title: responseData[key].title,
            amount: responseData[key].amount,
          });
        }
        dispatch({ type: 'INITIAL', ingredients: loadedIngredients });
      });
  }, []);

  useEffect(() => {
    console.log('rendering Ingredients...')
  }, [userIngredients])


  const filteredIngredientsHandler = useCallback(filteredIngredients => {
    dispatch({ type: 'SET', ingredients: filteredIngredients });
  }, []);

  const addIngredientHandler = useCallback(ingredient => {
    dispatchHttp({ type: 'SEND' });
    fetch('https://react-hooks-530fe.firebaseio.com/ingredients.json', {
      method: 'POST',
      body: JSON.stringify(ingredient),
      headers: { 'Content-Type': 'application/json' }
    }).then(response => {
      return response.json();
    }).then(responseData => {
      dispatchHttp({ type: 'RESPONSE' });
      dispatch({ type: 'ADD', ingredient: { id: responseData.name, ...ingredient } });
    }).catch(error => {
      dispatchHttp({ type: 'RESPONSE' });
      dispatchHttp({ type: 'ERROR', errorMessage: error.message });
    });

  }, []);

  const removeIngredientHandler = useCallback(ingredientID => {
    dispatchHttp({ type: 'SEND' });
    fetch(`https://react-hooks-530fe.firebaseio.com/ingredients/${ingredientID}.json`, {
      method: 'DELETE',
      headers: { "Access-Control-Allow-Credentials": true }
    }).then(response => {
      return response.json();
    }).then(responseData => {
      dispatchHttp({ type: 'RESPONSE' });
      dispatch({ type: 'DELETE', id: ingredientID });
    }).catch(error => {
      dispatchHttp({ type: 'RESPONSE' });
      dispatchHttp({ type: 'ERROR', errorMessage: error.message });
    });
  }, []);

  const clearError = () => {
    dispatchHttp({ type: 'CLEAR' });
  }

  const ingredientList =useMemo(()=>{
    return <IngredientList ingredients={userIngredients} onRemoveItem={removeIngredientHandler} />
  },[userIngredients,removeIngredientHandler])
  return (
    <div className="App">
      {httpState.error && <ErrorModal onClose={clearError}>{httpState.error}</ErrorModal>}
      <IngredientForm
        onAddIngredient={addIngredientHandler}
        loading={httpState.loading}
      />

      <section>
        <Search onLoadIngredients={filteredIngredientsHandler} />
        {ingredientList}
      </section>
    </div>
  );
}

export default Ingredients;
